# Changelog

## [1.0.2] - 2022-05-03

Package documentation has been updated.

## [1.0.1] - 2021-01-10

Package documentation has been updated.

## [1.0.0] - 2019-11-10

### Changed

- Function `alpy.utils.context_logger` has been split into two functions:
  `alpy.utils.make_context_logger` and `alpy.utils.context_logger`. The new
  `alpy.utils.context_logger` function has the same name but is simpler.

- Return value of function `alpy.container.stop` has changed. It returns
  container exit code.

### Removed

- Function `alpy.remote_shell.random_delimiter` has been removed.

## [0.23.0] - 2019-10-20

### Changed

- Functions working with container exit codes have been renamed. "status" code
  has been replaced with "exit" code. The new names are:
  `alpy.container.get_signal_number_from_exit_code`,
  `alpy.container.log_exit_code`, `alpy.container.check_exit_code`.

- Constant `alpy.console.PORT` has been moved to a new module `config`. The new
  name is `alpy.config.QEMU_SERIAL_PORT`.

## [0.22.0] - 2019-07-21

### Added

- Function `alpy.qemu.get_serial_port_args` has been modified so that raw
  console output is saved.

## [0.21.0] - 2019-07-13

### Added

- Context manager `alpy.console.connect` allows closing console connection
  within its *with* block.

- Function `alpy.utils.configure_logging` learned a new parameter
  `stderr_level`.

## [0.20.0] - 2019-07-06

This release makes the library more modular by cleanly separating qemu and
skeleton. Also, container management functionality has been reduced.

### Added

- Specify which signal to use in function `alpy.container.stop`.

### Changed

- Create instance of class `alpy.node.Skeleton` with function
  `alpy.node.make_skeleton` and pass it to function
  `alpy.run.qemu_with_skeleton`.

- Create instances of class `alpy.node.Node` with functions
  `alpy.node.make_node`, `alpy.node.make_numbered_nodes`.

- Functions `alpy.container.close`, `alpy.container.stop` do not remove
  containers.

- When container does not stop by itself function `alpy.container.close` kills
  the container with SIGKILL.

### Removed

- Remove functions `alpy.container.clean_start`, `alpy.container.run`.

## [0.19.0] - 2019-06-23

This release simplifies the library by removing a controller concept. The
controller was managing control flow and test resources. It had too many
responsibilities. It was not clear to the library user what it does.

### Added

- Add context managers `alpy.run.qemu_with_skeleton`,
  `alpy.utils.print_test_result`, `alpy.utils.trace_test_environment`.

### Removed

- Remove module `alpy.controller`.

## [0.18.0] - 2019-06-16

### Added

- Specify what busybox and iproute2 docker images should be used by the
  following classes: `alpy.node.Skeleton`, `alpy.node.NodeTap`,
  `alpy.node.Node`, `alpy.node.NodeContainer`.

- Add function `alpy.utils.signal_name`.

### Changed

- Extract class `alpy.controller.TestEnvironment`. Move test environment exit
  stack from `alpy.controller.ControllerBase._exit_stack` to
  `alpy.controller.ControllerBase._test_environment.exit_stack`.

- Remove `configure_interface` method from the following classes:
  `alpy.node.Skeleton`, `alpy.node.NodeTap`, `alpy.node.Node`. Add the following
  free functions: `alpy.container.configure_interface`,
  `alpy.container.add_ip_address`, `alpy.container.add_default_route`.

- When QEMU is killed by a signal or a container is killed by a signal print the
  signal name instead of printing the signal number.

- Convert `alpy.controller.ControllerBase.tap_interfaces` into a free function
  `alpy.controller.tap_interfaces`.

## [0.17.0] - 2019-06-01

With this release comes a rewrite of module *console*. Class
`alpy.console.SerialConsole` encapsulated a pexpect object. However, the object
was complex enough already. So the class has been split into free functions. The
functions just operate on the pexpect object directly.

### Added

- Add function `alpy.console.flush_log`.

- Support long filenames in functions `alpy.remote_shell.upload_text_file` and
  `alpy.remote_shell.execute_program`.

### Changed

- Move functions related to file upload and remote script execution to a new
  module `alpy.remote_shell`.

- Break up class `alpy.console.SerialConsole` into free functions.

- Use keyword arguments in the constructor of class `alpy.pexpect_log.Splitter`.

- Allow end of file in context manager `alpy.console.read_in_background`.

## [0.16.0] - 2019-05-18

### Added

- Support Python 3.6.

### Fixed

- Fix context manager `alpy.console.read_in_background`. Stop background thread
  when exiting the context with an exception.

- Fix context manager `alpy.qemu.temporary_copy_ovmf_vars`. Remove temporary
  file when exiting the context with an exception.

## [0.15.0] - 2019-05-11

### Added

- Functions `alpy.console.upload_and_execute_script`,
  `alpy.console.check_execute_program`, `alpy.console.execute_program` accept
  parameter `timeout`.

## [0.14.0] - 2019-05-05

### Added

- Add function `alpy.container.stop`.

### Changed

- Function `alpy.container.write_logs` logs both container standard output and
  error streams with level debug. The streams are labeled "stdout", "stderr" in
  the log.

## [0.13.0] - 2019-05-02

### Added

- Add functions `alpy.container.clean_start`, `alpy.container.close`.

- Add an optional flag to function `alpy.container.run`. The flag indicates that
  a container should be removed.

- Specify package dependencies.

## [0.12.0] - 2019-04-21

This release unifies logging output. Logger names are now derived from module
names.

### Added

- Add functions `alpy.console.upload_text_file`, `alpy.console.execute_program`,
  `alpy.console.check_execute_program`,
  `alpy.console.upload_and_execute_script`.

### Changed

- Replace `alpy.utils.step` logger with logger factory
  `alpy.utils.context_logger`.

## [0.11.0] - 2019-04-14

This release introduces a way to manage control flow of the test - using a
*Controller*. The library includes a base class for the controller.

### Added

- Add class `alpy.controller.ControllerBase`.

- Add context manager `alpy.qemu.run`.

- Add function `alpy.utils.configure_logging`.

- Add function `alpy.qemu.start_virtual_cpu`.

- Add context manager `alpy.qemu.temporary_copy_ovmf_vars`.

### Changed

- Add default console port 2023. Make arguments of
  `alpy.qemu.get_serial_port_args` and `alpy.console.SerialConsole.__init__`
  optional.

- Remove argument of `alpy.qemu.get_uefi_firmware_args` function.
  Use "OVMF_VARS.fd" file in the test directory.

### Removed

- Remove context managers `alpy.ContextBase`, `alpy.ContextWithTestResult`. Use
  `alpy.controller.ControllerBase` instead.

- Remove context managers `alpy.qemu.QemuProcess`, `alpy.qemu.QemuControlled`.
  Use `alpy.qemu.run` instead.

## [0.10.0] - 2019-04-07

The project is now mature enough to become a Python library for testing network
virtual appliances. It is no longer just a proof of concept.

"alp" name on PyPI is taken, so the project is renamed to *alpy*.

In short, "the project" is now Python library *alpy*.

### Changed

- Rename project to *alpy*.

- Switch to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

### Contributors

- Alexey Bogdanenko

## 9 - 2019-04-07

### Added

- Wrap long lines of console output. Detect long output sequences without line
  breaks and split them into small chunks.

- Add functions to upload a script via console, execute it and check error code.

- Install python on rabbit.

### Changed

- Rewrite rabbit configuration scripts in Python.

- Rewrite rabbit guest setup script in Python.

### Contributors

- Alexey Bogdanenko

## 8 - 2019-03-31

### Added

- Add test *forward-l2*.

- Stop container gracefully before removal.

- Split multiline console input logs.

### Changed

- Rename *skeleton container* to *node*.

- Import QMP Python module from PyPI.

- Increase the default timeout to accommodate for the default timeout of the
  ping utility.

- Move GitLab-specific CI scripts to ci/gitlab directory.

### Contributors

- Alexey Bogdanenko

## 7 - 2019-03-30

### Added

- Print either "Test passed" or "Test failed" at the end of a test.

- Write debug logs to "debug.log". Messages of higher priority go to stdout.

- Split logs of the *run-test* CI script. Debug logs go to "run-test.log".
  Messages of higher priority go to stdout.

### Changed

- Move `ContextBase`, `ContextWithTestResult` to the alp package.

- Use shorter, module-local names. For example, rename
  `alp.container.container_run` to `alp.container.run`.

### Contributors

- Alexey Bogdanenko

## 6 - 2019-03-17

Qemu-related but not Rabbit-specific code has been extracted from the rabbit_dut
package and moved to the `alp.qemu` module.

Resource management logic has been rewritten using `contextlib.ExitStack`.

### Added

- Add test *console-login*.

- Add test entry point: ContextLoggedIn.

- Expose console and qmp to the user.

### Changed

- Read console messages in background during shutdown.

### Contributors

- Alexey Bogdanenko

## 5 - 2019-03-16

This release adds support for testing rabbit with multiple network interfaces.

### Added

- Add test *forward-ipv4*.

### Changed

- Rename test *network* to *respond-to-ping*.

### Fixed

- Fix qemu arguments in multiple interfaces case.

- Create more than one interface.

### Contributors

- Alexey Bogdanenko

## 4 - 2019-03-10

The DUT is given a name - Rabbit. This helps differentiate between the DUT and
the test execution engine.

All but testcase-specific code has been moved from the first test to rabbit_dut
package. The first test is now just 34 lines long, which is great compared to
242 lines in v3.

### Added

- Add function rabbit_dut.context_configured to encapsulate test case context.
  The DUT enters this context right after executing configuration script.

- Add support of network appliances with multiple links to the alp package.

- Generate logger name for container output. Logger name is
  "container.{container_short_id}".

- Add class rabbit_dut.Params to gather in one place different test parameters.
  The parameters include the number of links and parameters derived from command
  line arguments.

- Add method NetworkSkeleton.configure_interface to configure skeleton network
  interface.

### Changed

- Rename package alpine_dut to rabbit_dut.

- Remove images exclusive to the test engine from docker-requirements file.
  The file should contain only images required by test case.

### Contributors

- Alexey Bogdanenko

## 3 - 2019-03-03

This release introduces two python packages: alp and alpine_dut. Alp package
contains code common for all tests and alpine_dut has DUT-specific code. The
interface provided by the packages is a work in progress, but most code has been
migrated from the first test to the packages.

### Added

- Check that source code files have the right license.

- Support multiple tap interfaces.

- Create packages alp and alpine_dut.

### Fixed

- Fix docker build failure. The failure was caused by PyPI missing binary
  packages. The fix is to install source packages.

- Fix docker build failure. The failure was caused by package version mismatch
  between the base image and Alpine repo. The fix is to upgrade before
  installing packages.

- Fix HDD image build failure. The problem was caused by the VM not having
  enough time to install packages.

- Remove tap interface when qemu fails to start.

- Fix race between skeleton container and docker event listener thread at
  startup.

- Fix CI scripts which failed to skip ".git" directory. It potentially resulted
  in git repository being corrupted by python formatter. Users are
  advised to replace the repository with a fresh copy.

### Contributors

- Alexey Bogdanenko

## 2 - 2019-02-24

### Added

- List docker images used by a test in a new "docker-requirements" file in the
  test directory. The images are pulled before running the test.

- Add check to make sure that docker daemon is up before running a test.

- Document network design of a test.

### Changed

- Switch from Docker CLI to Python API.

### Contributors

- Alexey Bogdanenko

## 1 - 2019-02-18

### Comment

This is the first release.

### Added

- Add script to build HDD image of a network appliance. The appliance is Alpine
  Linux plus a few packages.

- Add the first test. The test is called "network". The test boots and interacts
  with network virtual appliance (device under test, DUT). The test checks that
  DUT can be configured and responds to pings from a host.

- Add script to download the latest Alpine ISO.

- Add script to reformat Python code. Black code formatter is used.

- Add script to check code quality. The script calls Pylint. The script supports
  per-file whitelists of checks.

- Add GitLab CI configuration file.

- Add docker files. They include all dependencies needed to run build, test and
  CI scripts.

### Contributors

- Alexey Bogdanenko
