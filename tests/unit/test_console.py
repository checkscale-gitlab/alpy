# SPDX-License-Identifier: GPL-3.0-or-later

import logging
import socketserver
import threading
import time

import pexpect
import pytest

import alpy.console

PORT = 2379


class HelloHandler(socketserver.StreamRequestHandler):
    def handle(self):
        if self.rfile.readline() == b"hello\n":
            self.wfile.write(b"world\n")


class ReusableAddressTCPServer(socketserver.TCPServer):
    allow_reuse_address = True


@pytest.fixture
def hello_world_socket_server():
    server_address = ("127.0.0.1", PORT)
    with ReusableAddressTCPServer(server_address, HelloHandler) as server:
        server.timeout = 15
        thread = threading.Thread(target=server.handle_request)
        thread.start()
        yield
        thread.join()


class TestConnectSocket:
    @staticmethod
    def test_can_send_and_receive(
        hello_world_socket_server,
    ):  # pylint: disable=unused-argument
        sock = alpy.console.create_socket()
        alpy.console.connect_socket(sock, port=PORT)
        with sock:
            sock.sendall(b"hello\n")
            assert sock.recv(128) == b"world\n"


class TestFlushLog:
    @staticmethod
    def test_string_without_eol_is_logged(logger):
        console = pexpect.spawn("echo", ["-n", "hello"])
        console.logfile_read = alpy.pexpect_log.LogfileRead(logger)
        console.expect_exact("hello")
        alpy.console.flush_log(console)
        logger.debug.assert_called_once_with("< 'hello'")


class TestClose:
    @staticmethod
    def test_log_is_flushed(logger):
        console = pexpect.spawn("echo", ["hello"])
        console.logfile_read = alpy.pexpect_log.LogfileRead(logger)
        alpy.console.close(console)
        logger.debug.assert_called_once_with(r"< 'hello\r\n'")


class TestReadInBackground:
    @staticmethod
    def test_output_is_logged_in_background(logger):
        command = "echo one; sleep 2; echo two; sleep 2; echo three"
        console = pexpect.spawn("sh", ["-c", command])
        console.logfile_read = alpy.pexpect_log.LogfileRead(logger)

        with alpy.console.read_in_background(console):
            time.sleep(1)
            logger.debug.assert_called_with(r"< 'one\r\n'")
            time.sleep(2)
            logger.debug.assert_called_with(r"< 'two\r\n'")
            time.sleep(2)
            logger.debug.assert_called_with(r"< 'three\r\n'")


class TestConnect:
    @staticmethod
    def test_can_send_and_receive(
        hello_world_socket_server, caplog
    ):  # pylint: disable=unused-argument
        with alpy.console.connect(port=PORT, timeout=10) as console:
            caplog.set_level(logging.DEBUG)
            caplog.clear()
            console.sendline("hello")
            assert caplog.messages == [r"> 'hello\n'"]
            caplog.clear()
            console.expect_exact("world\n")
            assert caplog.messages == [r"< 'world\n'"]

    @staticmethod
    def test_can_close(
        hello_world_socket_server,
    ):  # pylint: disable=unused-argument
        with alpy.console.connect(port=PORT, timeout=10) as console:
            alpy.console.close(console)
