# SPDX-License-Identifier: GPL-3.0-or-later

import pytest

import alpy.pexpect_log


class TestSplitter:
    @staticmethod
    @pytest.fixture
    def on_chunk_ready(mocker):
        return mocker.stub()

    @staticmethod
    def test_when_input_is_chunk_separator_chunk(on_chunk_ready):

        splitter = alpy.pexpect_log.Splitter(
            sep=", ", chunk_ready_callback=on_chunk_ready, low=40, high=80
        )
        on_chunk_ready.assert_not_called()

        splitter.write("Hello, World!")
        on_chunk_ready.assert_called_once_with("Hello, ")
        assert on_chunk_ready.call_count == 1

        splitter.flush()
        on_chunk_ready.assert_called_with("World!")
        assert on_chunk_ready.call_count == 2

    @staticmethod
    def test_when_input_starts_with_separator(on_chunk_ready):

        splitter = alpy.pexpect_log.Splitter(
            sep=", ", chunk_ready_callback=on_chunk_ready, low=40, high=80
        )

        splitter.write(", ")
        on_chunk_ready.assert_called_once_with(", ")

    @staticmethod
    def test_extra_flush_does_nothing(on_chunk_ready):

        splitter = alpy.pexpect_log.Splitter(
            sep=", ", chunk_ready_callback=on_chunk_ready, low=40, high=80
        )

        splitter.write("Hello, World!")
        on_chunk_ready.assert_called_once_with("Hello, ")
        assert on_chunk_ready.call_count == 1

        splitter.flush()
        on_chunk_ready.assert_called_with("World!")
        assert on_chunk_ready.call_count == 2

        splitter.flush()
        assert on_chunk_ready.call_count == 2

    @staticmethod
    def test_can_write_after_flush(on_chunk_ready):

        splitter = alpy.pexpect_log.Splitter(
            sep=", ", chunk_ready_callback=on_chunk_ready, low=40, high=80
        )

        splitter.write("Hello, World!")
        on_chunk_ready.assert_called_once_with("Hello, ")
        assert on_chunk_ready.call_count == 1

        splitter.flush()
        on_chunk_ready.assert_called_with("World!")
        assert on_chunk_ready.call_count == 2

        splitter.write("Hi!")
        assert on_chunk_ready.call_count == 2

        splitter.flush()
        on_chunk_ready.assert_called_with("Hi!")
        assert on_chunk_ready.call_count == 3

    @staticmethod
    def test_can_flush_first(on_chunk_ready):
        splitter = alpy.pexpect_log.Splitter(
            sep=", ", chunk_ready_callback=on_chunk_ready, low=40, high=80
        )

        splitter.flush()
        splitter.flush()
        on_chunk_ready.assert_not_called()

    @staticmethod
    def test_when_input_is_fed_one_letter_at_a_time(on_chunk_ready):

        splitter = alpy.pexpect_log.Splitter(
            sep=", ", chunk_ready_callback=on_chunk_ready, low=40, high=80
        )

        splitter.write("H")
        splitter.write("e")
        splitter.write("l")
        splitter.write("l")
        splitter.write("o")
        splitter.write(",")
        on_chunk_ready.assert_not_called()

        splitter.write(" ")
        on_chunk_ready.assert_called_once_with("Hello, ")

    @staticmethod
    def test_low_and_high_parameters(on_chunk_ready, mocker):

        splitter = alpy.pexpect_log.Splitter(
            sep=", ", chunk_ready_callback=on_chunk_ready, low=2, high=5
        )

        splitter.write("C")
        splitter.write("h")
        splitter.write("a")
        splitter.write("i")
        on_chunk_ready.assert_not_called()

        splitter.write("r")
        assert on_chunk_ready.mock_calls == [
            mocker.call("Ch"),
            mocker.call("ai"),
        ]

        splitter.write(", ")
        on_chunk_ready.assert_called_with("r,")
        assert on_chunk_ready.call_count == 3

        splitter.flush()
        on_chunk_ready.assert_called_with(" ")
        assert on_chunk_ready.call_count == 4

    @staticmethod
    def test_when_input_has_multiple_long_sequences():
        chunks = []

        splitter = alpy.pexpect_log.Splitter(
            sep=", ", chunk_ready_callback=chunks.append, low=2, high=5
        )

        splitter.write("Ape, Bee, Cat, Deer, Eel, Fox, Gecko, Pig, Rat, ")
        assert chunks == [
            "Ape, ",
            "Bee, ",
            "Cat, ",
            "De",  # "Deer, " is too long. Switching to overflow mode.
            "er",
            ", ",  # Recovered from overflow mode.
            "Eel, ",
            "Fox, ",
            "Ge",  # "Gecko, " is too long. Switching to overflow mode.
            "ck",
            "o,",  # No recovery: no look-ahead.
            " P",
            # Perhaps, we should look back and detect the separator.
            # However, multi-character separator is currently not used.
            "ig",
            ", ",  # Recovered from overflow mode.
            "Rat, ",
        ]

    @staticmethod
    def test_when_input_is_long_string_without_separators():
        chunks = []
        splitter = alpy.pexpect_log.Splitter(
            sep=", ", chunk_ready_callback=chunks.append, low=5, high=7
        )
        splitter.write("0123456789abcde")
        assert chunks == ["01234", "56789", "abcde"]

    @staticmethod
    def test_when_low_and_high_parameters_are_equal(on_chunk_ready):

        splitter = alpy.pexpect_log.Splitter(
            sep=", ", chunk_ready_callback=on_chunk_ready, low=2, high=2
        )

        splitter.write("M")
        on_chunk_ready.assert_not_called()

        splitter.write("a")
        on_chunk_ready.assert_called_once_with("Ma")

        splitter.write("r")
        assert on_chunk_ready.call_count == 1

        splitter.write("s")
        on_chunk_ready.assert_called_with("rs")
        assert on_chunk_ready.call_count == 2


class TestFormatBytesForLogging:
    @staticmethod
    def test_text_is_quoted():
        assert alpy.pexpect_log.format_bytes_for_logging(b"Hello") == "'Hello'"

    @staticmethod
    def test_when_input_has_control_characters_then_output_is_hex():
        assert alpy.pexpect_log.format_bytes_for_logging(b"\x04") == r"'\x04'"
        assert (
            alpy.pexpect_log.format_bytes_for_logging(bytes([1, 2, 3]))
            == r"'\x01\x02\x03'"
        )


class TestLogfileRead:
    @staticmethod
    def test_when_input_is_one_full_line_and_one_partial_line(logger):
        logfile_read = alpy.pexpect_log.LogfileRead(logger)
        logfile_read.write(b"Hello\nWorld!")
        logger.debug.assert_called_once_with(r"< 'Hello\n'")
        logfile_read.log_remaining_text()
        logger.debug.assert_called_with("< 'World!'")
        assert logger.debug.call_count == 2

    @staticmethod
    def test_when_line_length_is_in_range_then_line_is_not_split(logger):
        logfile_read = alpy.pexpect_log.LogfileRead(logger)

        one_hundred_bytes = (
            b"Creating strings which are exactly 100 characters long is "
            b"difficult.  Length of this string is 100.\n"
        )
        assert len(one_hundred_bytes) == 100
        logfile_read.write(one_hundred_bytes)

        logger.debug.assert_called_once_with(
            "< 'Creating strings which are exactly 100 characters long is "
            r"difficult.  Length of this string is 100.\n'"
        )

    @staticmethod
    def test_when_input_has_no_line_breaks(logger, mocker):
        logfile_read = alpy.pexpect_log.LogfileRead(logger)

        two_hundred_bytes = (
            b"Creating strings which are two hundred characters long is "
            b"difficult. The key is to be creative. Using an editor which can "
            b"count characters also helps. Anyway I am almost done. One last "
            b"sentence. Blah."
        )

        logfile_read.write(two_hundred_bytes)
        assert logger.debug.mock_calls == [
            mocker.call("< 'Creating strings whi'"),
            mocker.call("< 'ch are two hundred c'"),
            mocker.call("< 'haracters long is di'"),
            mocker.call("< 'fficult. The key is '"),
            mocker.call("< 'to be creative. Usin'"),
            mocker.call("< 'g an editor which ca'"),
            mocker.call("< 'n count characters a'"),
            mocker.call("< 'lso helps. Anyway I '"),
            mocker.call("< 'am almost done. One '"),
            mocker.call("< 'last sentence. Blah.'"),
        ]


class TestLogfileSendSplitLines:
    # pylint: disable=protected-access
    @staticmethod
    def test_when_input_is_empty():
        assert alpy.pexpect_log.LogfileSend._split_lines(b"") == [b""]

    @staticmethod
    def test_when_input_has_no_line_break():
        assert alpy.pexpect_log.LogfileSend._split_lines(b"hello") == [b"hello"]

    @staticmethod
    def test_when_input_is_one_line():
        assert alpy.pexpect_log.LogfileSend._split_lines(b"hello\n") == [
            b"hello\n"
        ]

    @staticmethod
    def test_when_input_is_separator():
        assert alpy.pexpect_log.LogfileSend._split_lines(b"\n") == [b"\n"]

    @staticmethod
    def test_when_input_is_two_separators():
        assert alpy.pexpect_log.LogfileSend._split_lines(b"\n\n") == [
            b"\n",
            b"\n",
        ]

    @staticmethod
    def test_when_input_is_one_full_line_and_one_partial_line():
        assert alpy.pexpect_log.LogfileSend._split_lines(b"hello\nworld") == [
            b"hello\n",
            b"world",
        ]

    @staticmethod
    def test_when_input_is_two_lines():
        assert alpy.pexpect_log.LogfileSend._split_lines(b"hello\nworld\n") == [
            b"hello\n",
            b"world\n",
        ]


class TestLogfileSend:
    @staticmethod
    def test_when_input_is_one_full_line_and_one_partial_line(logger, mocker):
        logfile_send = alpy.pexpect_log.LogfileSend(logger)

        logfile_send.write(b"Hello\nWorld!")

        assert logger.debug.mock_calls == [
            mocker.call(r"> 'Hello\n'"),
            mocker.call("> 'World!'"),
        ]
