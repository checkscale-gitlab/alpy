# SPDX-License-Identifier: GPL-3.0-or-later

import docker
import pytest


@pytest.fixture
def logger(mocker):
    return mocker.NonCallableMock(spec_set=["debug"])


@pytest.fixture(scope="session")
def docker_client():
    client = docker.from_env()
    yield client
    client.close()
