# SPDX-License-Identifier: GPL-3.0-or-later

import logging
import signal
import threading
import time
import unittest.mock

import pytest
import requests.exceptions

import alpy.container
import common


class TestWriteLogs:
    @staticmethod
    @pytest.fixture
    def echo_hello_container(docker_client):
        container = docker_client.containers.create(
            common.IMAGE_BUSYBOX, ["echo", "hello"]
        )
        container.start()
        container.wait()
        yield container
        container.remove()

    @staticmethod
    def test_when_container_writes_to_stdout_then_output_is_logged(
        echo_hello_container, logger, mocker
    ):
        with unittest.mock.patch(
            "logging.getLogger", new=mocker.Mock(return_value=logger)
        ):
            alpy.container.write_logs(echo_hello_container)

        logger.debug.assert_called_once_with(
            f"{echo_hello_container.short_id} stdout: hello"
        )

    @staticmethod
    @pytest.fixture
    def echo_hello_stderr_container(docker_client):
        container = docker_client.containers.create(
            common.IMAGE_BUSYBOX, ["sh", "-c", "echo hello 1>&2"]
        )
        container.start()
        container.wait()
        yield container
        container.remove()

    @staticmethod
    def test_when_container_writes_to_stderr_then_output_is_logged(
        echo_hello_stderr_container, logger, mocker
    ):
        with unittest.mock.patch(
            "logging.getLogger", new=mocker.Mock(return_value=logger)
        ):
            alpy.container.write_logs(echo_hello_stderr_container)

        logger.debug.assert_called_once_with(
            f"{echo_hello_stderr_container.short_id} stderr: hello"
        )


class TestLogExitCode:
    @staticmethod
    def test_when_exit_code_is_zero_then_exit_code_is_logged(caplog):
        caplog.set_level(logging.DEBUG)
        alpy.container.log_exit_code(0, "apache")
        assert caplog.messages == ["Container apache exited with code 0"]

    @staticmethod
    def test_when_exit_code_is_not_zero_then_exit_code_is_logged(caplog):
        caplog.set_level(logging.DEBUG)
        alpy.container.log_exit_code(1, "apache")
        assert caplog.messages == ["Container apache exited with code 1"]

    @staticmethod
    def test_when_exit_code_means_signal_then_signal_name_is_logged(caplog):
        caplog.set_level(logging.DEBUG)
        alpy.container.log_exit_code(128 + signal.SIGKILL, "apache")
        assert caplog.messages == [
            "Container apache was killed by signal SIGKILL"
        ]


class TestCheckExitCode:
    @staticmethod
    def test_when_exit_code_is_zero_then_no_exception_is_raised():
        alpy.container.check_exit_code(0)

    @staticmethod
    def test_when_exit_code_is_not_zero_then_exception_is_raised():
        with pytest.raises(alpy.utils.NonZeroExitCode) as exception_info:
            alpy.container.check_exit_code(1)
        assert (
            str(exception_info.value)
            == "Container process exited with non-zero code 1"
        )

    @staticmethod
    def test_when_exit_code_means_signal_then_exception_is_raised():
        with pytest.raises(alpy.utils.NonZeroExitCode) as exception_info:
            alpy.container.check_exit_code(128 + signal.SIGKILL)
        assert (
            str(exception_info.value)
            == "Container process was killed by signal SIGKILL"
        )


class TestStop:
    @staticmethod
    @pytest.fixture
    def running_container(docker_client):
        container = docker_client.containers.create(
            common.IMAGE_BUSYBOX, ["cat"], init=True, stdin_open=True
        )
        container.start()
        time.sleep(3)
        container.reload()
        assert container.status == "running"
        yield container
        container.reload()
        if container.status == "running":
            container.kill()
            container.wait()
        container.remove()

    @staticmethod
    def test_container_is_stopped(running_container):
        container = running_container
        alpy.container.stop(container, timeout=5)
        container.reload()
        assert container.status != "running"

    @staticmethod
    def test_signal_number_is_returned(running_container):
        container = running_container
        code = alpy.container.stop(container, timeout=5, signal="SIGUSR1")
        signal_number = alpy.container.get_signal_number_from_exit_code(code)
        assert signal_number == signal.SIGUSR1

    @staticmethod
    def test_container_is_killed_with_sigterm_by_default(running_container):
        container = running_container
        code = alpy.container.stop(container, timeout=5)
        signal_number = alpy.container.get_signal_number_from_exit_code(code)
        assert signal_number == signal.SIGTERM

    @staticmethod
    @pytest.fixture
    def running_container_which_ignores_signals(docker_client):
        container = docker_client.containers.create(
            common.IMAGE_BUSYBOX, ["cat"], stdin_open=True
        )

        # Note that this container ignores SIGTERM.
        #
        # See https://docs.docker.com/engine/reference/run/#foreground:
        #
        #     A process running as PID 1 inside a container is treated specially
        #     by Linux: it ignores any signal with the default action. So, the
        #     process will not terminate on SIGINT or SIGTERM unless it is coded
        #     to do so.
        container.start()
        time.sleep(3)
        container.reload()
        assert container.status == "running"
        yield container
        container.reload()
        if container.status == "running":
            container.kill()
            container.wait()
        container.remove()

    @staticmethod
    def test_when_container_does_not_stop_then_exception_is_raised(
        running_container_which_ignores_signals,
    ):
        container = running_container_which_ignores_signals
        with pytest.raises(requests.exceptions.ConnectionError):
            alpy.container.stop(container, timeout=3)
        container.reload()
        assert container.status == "running"

    @staticmethod
    def test_timeout_parameter(running_container_which_ignores_signals):
        container = running_container_which_ignores_signals
        start_time = time.time()
        with pytest.raises(requests.exceptions.ConnectionError):
            alpy.container.stop(container, timeout=3)
        assert 3 < time.time() - start_time < 6

    @staticmethod
    @pytest.fixture
    def compute_two_plus_two_container(docker_client):
        container = docker_client.containers.create(
            common.IMAGE_BUSYBOX,
            ["sh", "-c", "echo 2+2=$((2+2)) && exec cat"],
            init=True,
            stdin_open=True,
        )
        container.start()
        time.sleep(3)
        container.reload()
        assert container.status == "running"
        yield container
        container.reload()
        if container.status == "running":
            container.kill()
            container.wait()
        container.remove()

    @staticmethod
    def test_container_output_is_logged(compute_two_plus_two_container, caplog):
        caplog.set_level(logging.DEBUG)
        alpy.container.stop(compute_two_plus_two_container, timeout=5)
        assert "2+2=4" in caplog.text

    @staticmethod
    @pytest.fixture
    def on_sigterm_compute_two_plus_two_container(docker_client):
        command = (
            "trap 'echo 2+2=$((2+2)) && exit' SIGTERM; "
            "while true; do sleep 1; done"
        )
        container = docker_client.containers.create(
            common.IMAGE_BUSYBOX, ["sh", "-c", command], stdin_open=True
        )
        container.start()
        time.sleep(3)
        container.reload()
        assert container.status == "running"
        yield container
        container.reload()
        if container.status == "running":
            container.kill()
            container.wait()
        container.remove()

    @staticmethod
    def test_when_container_writes_after_signal_then_output_is_logged(
        on_sigterm_compute_two_plus_two_container, caplog
    ):
        caplog.set_level(logging.DEBUG)
        alpy.container.stop(
            on_sigterm_compute_two_plus_two_container, timeout=5
        )
        assert "2+2=4" in caplog.text

    @staticmethod
    def test_signal_name_is_logged(running_container, caplog):
        caplog.set_level(logging.DEBUG)
        alpy.container.stop(running_container, timeout=5)
        assert "was killed by signal SIGTERM" in caplog.text


class TestClose:
    @staticmethod
    @pytest.fixture
    def three_second_container(docker_client):
        container = docker_client.containers.create(
            common.IMAGE_BUSYBOX, ["sleep", "3"]
        )
        container.start()
        yield container
        container.wait()
        container.remove()

    @staticmethod
    def test_container_is_waited_for(three_second_container):
        container = three_second_container
        alpy.container.close(container, timeout=10)
        container.reload()
        assert container.status != "running"

    @staticmethod
    @pytest.fixture
    def compute_two_plus_two_container(docker_client):
        container = docker_client.containers.create(
            common.IMAGE_BUSYBOX, ["sh", "-c", "echo 2+2=$((2+2))"]
        )
        container.start()
        yield container
        container.wait()
        container.remove()

    @staticmethod
    def test_container_output_is_logged(compute_two_plus_two_container, caplog):
        caplog.set_level(logging.DEBUG)
        alpy.container.close(compute_two_plus_two_container, timeout=10)
        assert "2+2=4" in caplog.text

    @staticmethod
    @pytest.fixture
    def delayed_compute_two_plus_two_container(docker_client):
        container = docker_client.containers.create(
            common.IMAGE_BUSYBOX, ["sh", "-c", "sleep 2 && echo 2+2=$((2+2))"]
        )
        container.start()
        yield container
        container.wait()
        container.remove()

    @staticmethod
    def test_when_container_writes_after_calling_close_then_output_is_logged(
        delayed_compute_two_plus_two_container, caplog
    ):
        caplog.set_level(logging.DEBUG)
        alpy.container.close(delayed_compute_two_plus_two_container, timeout=10)
        assert "2+2=4" in caplog.text

    @staticmethod
    @pytest.fixture
    def false_container(docker_client):
        container = docker_client.containers.create(
            common.IMAGE_BUSYBOX, ["false"]
        )
        container.start()
        yield container
        container.wait()
        container.remove()

    @staticmethod
    def test_when_container_exit_code_is_not_zero_then_exception_is_raised(
        false_container,
    ):
        with pytest.raises(alpy.utils.NonZeroExitCode) as exception_info:
            alpy.container.close(false_container, timeout=10)
        assert (
            str(exception_info.value)
            == "Container process exited with non-zero code 1"
        )

    @staticmethod
    def test_exit_code_is_logged(false_container, caplog):
        caplog.set_level(logging.DEBUG)
        with pytest.raises(alpy.utils.NonZeroExitCode):
            alpy.container.close(false_container, timeout=10)
        assert "exited with code 1" in caplog.text

    @staticmethod
    @pytest.fixture
    def running_container(docker_client):
        container = docker_client.containers.create(
            common.IMAGE_BUSYBOX, ["cat"], stdin_open=True
        )
        container.start()
        yield container
        container.reload()
        if container.status == "running":
            container.kill()
            container.wait()
        container.remove()

    @staticmethod
    def test_when_timeout_then_container_is_killed(running_container, caplog):
        container = running_container
        caplog.set_level(logging.DEBUG)
        with pytest.raises(requests.exceptions.ConnectionError):
            alpy.container.close(container, timeout=3)
        container.reload()
        assert container.status != "running"
        assert "was killed by signal SIGKILL" in caplog.text

    @staticmethod
    def test_when_timeout_then_exception_is_raised(running_container):
        with pytest.raises(requests.exceptions.ConnectionError):
            alpy.container.close(running_container, timeout=3)

    @staticmethod
    def test_timeout_parameter(running_container):
        start_time = time.time()
        with pytest.raises(requests.exceptions.ConnectionError):
            alpy.container.close(running_container, timeout=3)
        assert 3 < time.time() - start_time < 6

    @staticmethod
    def test_when_timeout_then_timeout_is_logged(running_container, caplog):
        caplog.set_level(logging.DEBUG)
        with pytest.raises(requests.exceptions.ConnectionError):
            alpy.container.close(running_container, timeout=3)
        assert "Timed out waiting for container" in caplog.text


class TestWaitRunning:
    @staticmethod
    @pytest.fixture
    def delayed_start_container(docker_client):
        container = docker_client.containers.create(
            common.IMAGE_BUSYBOX, ["cat"], stdin_open=True
        )
        try:
            timer = threading.Timer(2, container.start)
            timer.start()
            yield container
            timer.join()
        finally:
            time.sleep(1)  # wait for container to start
            container.kill()
            container.wait()
            container.remove()

    @staticmethod
    def test_container_is_running(delayed_start_container):
        container = delayed_start_container
        alpy.container.wait_running(container, 10)
        container.reload()
        assert container.status == "running"

    @staticmethod
    @pytest.fixture
    def not_running_container(docker_client):
        container = docker_client.containers.create(common.IMAGE_BUSYBOX)
        yield container
        container.remove()

    @staticmethod
    def test_when_timeout_then_exception_is_raised(not_running_container):
        with pytest.raises(alpy.container.Timeout):
            alpy.container.wait_running(not_running_container, 2)

    @staticmethod
    def test_when_timeout_then_duration_is_correct(not_running_container):
        start_time = time.time()
        with pytest.raises(alpy.container.Timeout):
            alpy.container.wait_running(not_running_container, 2)
        assert 2 < time.time() - start_time < 5

    @staticmethod
    def test_when_no_timeout_then_return_before_timeout(
        delayed_start_container,
    ):
        start_time = time.time()
        alpy.container.wait_running(delayed_start_container, 10)
        assert time.time() < start_time + 5


@pytest.fixture
def container_with_interface(docker_client):

    command = (
        "ip link add dev eth0 type dummy"
        " && "
        "ip link set up dev eth0"
        " && "
        "exec cat"
    )

    container = docker_client.containers.create(
        common.IMAGE_BUSYBOX,
        ["sh", "-c", command],
        auto_remove=True,
        cap_add=["NET_ADMIN"],
        network_mode="none",
        stdin_open=True,
    )

    container.start()
    time.sleep(3)
    container.reload()
    assert container.status == "running"
    yield container
    container.kill()


class TestAddIpAddress:
    @staticmethod
    def test_ip_address_is_assigned(docker_client, container_with_interface):

        alpy.container.add_ip_address(
            container_with_interface.name,
            "192.168.1.2/24",
            docker_client=docker_client,
            image=common.IMAGE_BUSYBOX,
            timeout=5,
        )

        output = docker_client.containers.run(
            common.IMAGE_BUSYBOX,
            ["ip", "address", "show", "dev", "eth0"],
            network_mode="container:" + container_with_interface.name,
            remove=True,
        )

        assert b"inet 192.168.1.2/24" in output


class TestAddDefaultRoute:
    @staticmethod
    def test_default_route_exists(docker_client, container_with_interface):

        alpy.container.add_ip_address(
            container_with_interface.name,
            "192.168.1.2/24",
            docker_client=docker_client,
            image=common.IMAGE_BUSYBOX,
            timeout=5,
        )

        alpy.container.add_default_route(
            container_with_interface.name,
            "192.168.1.1",
            docker_client=docker_client,
            image=common.IMAGE_BUSYBOX,
            timeout=5,
        )

        output = docker_client.containers.run(
            common.IMAGE_BUSYBOX,
            ["ip", "route", "show"],
            network_mode="container:" + container_with_interface.name,
            remove=True,
        )

        assert b"default via 192.168.1.1 dev eth0" in output


class TestConfigureInterface:
    @staticmethod
    def test_ip_address_is_assigned(docker_client, container_with_interface):

        alpy.container.configure_interface(
            container_with_interface.name,
            "192.168.1.2/24",
            "192.168.1.1",
            docker_client=docker_client,
            image=common.IMAGE_BUSYBOX,
            timeout=5,
        )

        output = docker_client.containers.run(
            common.IMAGE_BUSYBOX,
            ["ip", "address", "show", "dev", "eth0"],
            network_mode="container:" + container_with_interface.name,
            remove=True,
        )

        assert b"inet 192.168.1.2/24" in output

    @staticmethod
    def test_default_route_exists(docker_client, container_with_interface):

        alpy.container.configure_interface(
            container_with_interface.name,
            "192.168.1.2/24",
            "192.168.1.1",
            docker_client=docker_client,
            image=common.IMAGE_BUSYBOX,
            timeout=5,
        )

        output = docker_client.containers.run(
            common.IMAGE_BUSYBOX,
            ["ip", "route", "show"],
            network_mode="container:" + container_with_interface.name,
            remove=True,
        )

        assert b"default via 192.168.1.1 dev eth0" in output
