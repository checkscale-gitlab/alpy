# SPDX-License-Identifier: GPL-3.0-or-later

import logging
import subprocess
import sys

import pytest

import alpy.utils


class TestConfigureLogging:
    @staticmethod
    def test_log_file_exists(tmp_path):
        program = (
            "import os\n"
            "import alpy.utils\n"
            f"os.chdir('{tmp_path}')\n"
            "alpy.utils.configure_logging()\n"
        )
        subprocess.run([sys.executable, "-c", program], check=True)
        assert (tmp_path / "debug.log").exists()

    @staticmethod
    def test_when_message_is_logged_then_log_file_contains_the_message(
        tmp_path,
    ):
        program = (
            "import logging\n"
            "import os\n"
            "import alpy.utils\n"
            f"os.chdir('{tmp_path}')\n"
            "alpy.utils.configure_logging()\n"
            "logging.debug('hello')\n"
        )
        subprocess.run([sys.executable, "-c", program], check=True)
        assert "hello" in (tmp_path / "debug.log").read_text()

    @staticmethod
    def test_when_by_default_then_info_message_is_printed_to_stderr(tmp_path):
        program = (
            "import logging\n"
            "import os\n"
            "import alpy.utils\n"
            f"os.chdir('{tmp_path}')\n"
            "alpy.utils.configure_logging()\n"
            "logging.info('hello')\n"
        )
        completed_process = subprocess.run(
            [sys.executable, "-c", program],
            capture_output=True,
            check=True,
            universal_newlines=True,
        )
        assert "hello" in completed_process.stderr

    @staticmethod
    def test_when_by_default_then_debug_message_is_not_printed(tmp_path):
        program = (
            "import logging\n"
            "import os\n"
            "import alpy.utils\n"
            f"os.chdir('{tmp_path}')\n"
            "alpy.utils.configure_logging()\n"
            "logging.debug('hello')\n"
        )
        completed_process = subprocess.run(
            [sys.executable, "-c", program],
            capture_output=True,
            check=True,
            universal_newlines=True,
        )
        assert not completed_process.stdout
        assert not completed_process.stderr

    @staticmethod
    def test_when_level_is_debug_then_debug_message_is_printed_to_stderr(
        tmp_path,
    ):
        program = (
            "import logging\n"
            "import os\n"
            "import alpy.utils\n"
            f"os.chdir('{tmp_path}')\n"
            "alpy.utils.configure_logging(stderr_level=logging.DEBUG)\n"
            "logging.debug('hello')\n"
        )
        completed_process = subprocess.run(
            [sys.executable, "-c", program],
            capture_output=True,
            check=True,
            universal_newlines=True,
        )
        assert "hello" in completed_process.stderr


class TestContextLogger:
    @staticmethod
    def test_enter_context_message_is_logged(mocker):
        logger = mocker.NonCallableMock(spec_set=["info"])
        with alpy.utils.context_logger(logger, "Add water"):
            logger.info.assert_called_once_with("Add water...")

    @staticmethod
    def test_when_context_is_exited_without_exception_then_success_is_logged(
        mocker,
    ):
        logger = mocker.NonCallableMock(spec_set=["info"])
        with alpy.utils.context_logger(logger, "Add water"):
            logger.info.reset_mock()
        logger.info.assert_called_once_with("Add water... done")

    @staticmethod
    def test_when_context_is_exited_with_exception_then_failure_is_logged(
        mocker,
    ):
        logger = mocker.NonCallableMock(spec_set=["info", "error"])

        class NoSuchIngredient(Exception):
            pass

        with pytest.raises(NoSuchIngredient):
            with alpy.utils.context_logger(logger, "Add water"):
                raise NoSuchIngredient

        logger.error.assert_called_once_with("Add water... failed")


class TestSignalName:
    @staticmethod
    def test_signal_name_is_returned():
        assert alpy.utils.signal_name(9) == "SIGKILL"


class TestPrintTestResult:
    @staticmethod
    def test_no_messages_are_logged_on_entering_context(caplog):
        caplog.set_level(logging.DEBUG)
        with alpy.utils.print_test_result():
            assert caplog.record_tuples == []

    @staticmethod
    def test_when_context_is_exited_without_exception_then_success_is_logged(
        caplog,
    ):
        caplog.set_level(logging.DEBUG)
        with alpy.utils.print_test_result():
            pass
        assert len(caplog.record_tuples) == 1
        assert caplog.record_tuples[0][1:] == (logging.INFO, "Test passed")

    @staticmethod
    def test_when_context_is_exited_with_exception_then_failure_is_logged(
        caplog,
    ):
        caplog.set_level(logging.DEBUG)
        with pytest.raises(RuntimeError):
            with alpy.utils.print_test_result():
                raise RuntimeError()
        assert len(caplog.record_tuples) == 1
        assert caplog.record_tuples[0][1:] == (logging.ERROR, "Test failed")


class TestTraceTestEnvironment:
    @staticmethod
    def test_enter_context_message_is_logged(caplog):
        caplog.set_level(logging.DEBUG)
        with alpy.utils.trace_test_environment():
            assert len(caplog.record_tuples) == 1
            assert caplog.record_tuples[0][1:] == (
                logging.INFO,
                "Enter test environment",
            )

    @staticmethod
    def test_when_context_is_exited_without_exception_then_success_is_logged(
        caplog,
    ):
        caplog.set_level(logging.DEBUG)
        with alpy.utils.trace_test_environment():
            caplog.clear()
        assert len(caplog.record_tuples) == 1
        assert caplog.record_tuples[0][1:] == (
            logging.INFO,
            "Exit test environment with success",
        )

    @staticmethod
    def test_when_context_is_exited_with_exception_then_failure_is_logged(
        caplog,
    ):
        caplog.set_level(logging.DEBUG)
        with pytest.raises(RuntimeError):
            with alpy.utils.trace_test_environment():
                caplog.clear()
                raise RuntimeError()
        assert len(caplog.record_tuples) == 1
        assert caplog.record_tuples[0][1:] == (
            logging.ERROR,
            "Exit test environment with failure",
        )
