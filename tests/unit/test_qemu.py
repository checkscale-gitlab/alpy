# SPDX-License-Identifier: GPL-3.0-or-later

import logging
import socket
import subprocess
import sys
import time

import pexpect.fdpexpect
import pytest

import alpy.qemu
import alpy.utils


class TestRun:
    @staticmethod
    def test_can_interact_with_qemu():
        args = [
            "qemu-system-x86_64",
            "-S",
            "-no-shutdown",
            "-nodefaults",
            "-m",
            "64",
            "-display",
            "none",
        ] + alpy.qemu.get_qmp_args()
        with alpy.qemu.run(args, timeout=10) as qmp:
            assert "qemu" in qmp.command("query-version")


class TestStartVirtualCpu:
    @staticmethod
    def test_cpu_starts():
        args = [
            "qemu-system-x86_64",
            "-S",
            "-no-shutdown",
            "-nodefaults",
            "-m",
            "64",
            "-display",
            "none",
        ] + alpy.qemu.get_qmp_args()
        with alpy.qemu.run(args, timeout=10) as qmp:
            assert not qmp.command("query-status")["running"]
            alpy.qemu.start_virtual_cpu(qmp)
            assert qmp.command("query-status")["running"]


class TestReadEvents:
    @staticmethod
    def test_events_are_logged(caplog):
        caplog.set_level(logging.DEBUG)
        host = "127.0.0.1"
        port = 2023
        args = [
            "qemu-system-x86_64",
            "-S",
            "-no-shutdown",
            "-nodefaults",
            "-m",
            "64",
            "-display",
            "none",
            "-chardev",
            f"socket,id=id_char_hmp,port={port},host={host},ipv4,nodelay,server,nowait",
            "-mon",
            "chardev=id_char_hmp",
        ] + alpy.qemu.get_qmp_args()
        with alpy.qemu.run(args, timeout=10) as qmp:
            sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            sock.connect((host, port))
            monitor = pexpect.fdpexpect.fdspawn(sock, timeout=10)
            try:
                prompt = "(qemu) "
                monitor.expect_exact(prompt)
                monitor.sendline("cont")
                monitor.expect_exact(prompt)
                monitor.sendline("stop")
                monitor.expect_exact(prompt)
                caplog.clear()
                alpy.qemu.read_events(qmp)
                qmp_log_messages = [
                    record_tuple[2]
                    for record_tuple in caplog.record_tuples
                    if record_tuple[0] == "QMP"
                ]
            finally:
                monitor.close()

        assert any(
            "'event': 'RESUME'" in message for message in qmp_log_messages
        )
        assert any("'event': 'STOP'" in message for message in qmp_log_messages)


class TestClose:
    @staticmethod
    @pytest.fixture
    def true_process():
        with subprocess.Popen(
            ["true"], stdout=subprocess.PIPE, stderr=subprocess.PIPE
        ) as process:
            yield process

    @staticmethod
    def test_process_exited(true_process):
        alpy.qemu.close(true_process, timeout=5)
        assert true_process.poll() == 0

    @staticmethod
    @pytest.fixture
    def one_second_process():
        with subprocess.Popen(
            ["sleep", "1"], stdout=subprocess.PIPE, stderr=subprocess.PIPE
        ) as process:
            yield process

    @staticmethod
    def test_long_process_is_waited_for(one_second_process):
        alpy.qemu.close(one_second_process, timeout=5)
        assert one_second_process.poll() == 0

    @staticmethod
    def test_long_process_not_waited_for_longer_than_necessary(
        one_second_process,
    ):
        start_time = time.time()
        alpy.qemu.close(one_second_process, timeout=5)
        assert time.time() - start_time < 2

    @staticmethod
    @pytest.fixture
    def false_process():
        with subprocess.Popen(
            ["false"], stdout=subprocess.PIPE, stderr=subprocess.PIPE
        ) as process:
            yield process

    @staticmethod
    def test_when_process_exits_with_nonzero_code_then_exception_is_raised(
        false_process,
    ):
        with pytest.raises(alpy.utils.NonZeroExitCode) as exception_info:
            alpy.qemu.close(false_process, timeout=5)

        assert str(exception_info.value) == "QEMU exited with non-zero code 1"

    @staticmethod
    @pytest.fixture
    def ten_second_process():
        with subprocess.Popen(
            ["sleep", "10"], stdout=subprocess.PIPE, stderr=subprocess.PIPE
        ) as process:
            yield process

    @staticmethod
    def test_when_timeout_then_process_is_killed(ten_second_process):
        with pytest.raises(alpy.utils.NonZeroExitCode):
            alpy.qemu.close(ten_second_process, timeout=0)
        assert ten_second_process.poll() < 0

    @staticmethod
    def test_when_timeout_then_exception_is_raised(ten_second_process):
        with pytest.raises(alpy.utils.NonZeroExitCode) as exception_info:
            alpy.qemu.close(ten_second_process, timeout=0)
        assert str(exception_info.value) == "QEMU was killed by signal SIGKILL"

    @staticmethod
    def test_when_timeout_then_message_is_logged(ten_second_process, caplog):
        with pytest.raises(alpy.utils.NonZeroExitCode):
            alpy.qemu.close(ten_second_process, timeout=1)
        assert caplog.messages == ["QEMU is still running"]

    @staticmethod
    def test_timeout_parameter(ten_second_process):
        start_time = time.time()
        with pytest.raises(alpy.utils.NonZeroExitCode):
            alpy.qemu.close(ten_second_process, timeout=1)
        assert 1 < time.time() - start_time < 2

    @staticmethod
    def test_exit_code_is_logged(true_process, caplog):
        caplog.set_level(logging.DEBUG)
        alpy.qemu.close(true_process, timeout=5)
        assert caplog.messages == ["QEMU exited with code 0"]

    @staticmethod
    @pytest.fixture
    def hello_process():
        with subprocess.Popen(
            ["echo", "hello"],
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
            universal_newlines=True,
        ) as process:
            yield process

    @staticmethod
    def test_when_process_writes_to_stdout_then_output_is_logged(
        hello_process, caplog
    ):
        caplog.set_level(logging.DEBUG)
        alpy.qemu.close(hello_process, timeout=5)
        assert "qemu stdout: hello" in caplog.messages

    @staticmethod
    @pytest.fixture
    def hello_stderr_process():
        with subprocess.Popen(
            [
                sys.executable,
                "-c",
                "import sys; print('hello', file=sys.stderr)",
            ],
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
            universal_newlines=True,
        ) as process:
            yield process

    @staticmethod
    def test_when_process_writes_to_stderr_then_output_is_logged(
        hello_stderr_process, caplog
    ):
        caplog.set_level(logging.DEBUG)
        alpy.qemu.close(hello_stderr_process, timeout=5)
        assert "qemu stderr: hello" in caplog.messages


class TestWaitShutdown:
    @staticmethod
    def test_when_qemu_emits_shutdown_and_stop_events_then_function_returns(
        mocker,
    ):
        qmp = mocker.NonCallableMock(spec_set=["pull_event"])
        qmp.pull_event = mocker.Mock(
            side_effect=[{"event": "SHUTDOWN"}, {"event": "STOP"}]
        )
        alpy.qemu.wait_shutdown(qmp)
        assert qmp.pull_event.call_count == 2

    @staticmethod
    def test_when_qemu_emits_other_events_then_other_events_are_ignored(mocker):
        qmp = mocker.NonCallableMock(spec_set=["pull_event"])
        qmp.pull_event = mocker.Mock(
            side_effect=[
                {"event": "some_event1"},
                {"event": "SHUTDOWN"},
                {"event": "some_event2"},
                {"event": "STOP"},
            ]
        )
        alpy.qemu.wait_shutdown(qmp)
        assert qmp.pull_event.call_count == 4
