# SPDX-License-Identifier: GPL-3.0-or-later

"""This module contains shared constants."""

QEMU_SERIAL_PORT = 2023
"""Default TCP port for QEMU to listen on and redirect virtual serial port to"""
