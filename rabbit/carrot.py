# SPDX-License-Identifier: GPL-3.0-or-later

import argparse
import collections
import contextlib
import os

import alpy.console
import alpy.node
import alpy.qemu
import alpy.remote_shell
import alpy.run
import alpy.utils
import docker

import alpine

PROMPT = "rabbit:~# "
IMAGE_BUSYBOX = "busybox:latest"


def add_common_arguments(parser):

    parser.add_argument(
        "--disable-kvm",
        help="Disable KVM full virtualization",
        action="store_true",
    )

    parser.add_argument(
        "--timeout",
        type=int,
        help=(
            "Base timeout for interactive operations, in seconds. "
            "For convenience, the default timeout should accommodate for the "
            "default timeout of the ping utility. "
            'See "busybox ping --help". '
            "Default: %(default)s"
        ),
        default=15,
    )

    parser.add_argument(
        "--hdd-image-path", required=True, help="HDD image to boot from"
    )


def get_qemu_args(*, enable_kvm, hdd_image_path, tap_interfaces):

    args = ["qemu-system-x86_64"]

    # General
    # -------

    args.append("-S")
    args.append("-no-shutdown")
    args.append("-nodefaults")
    if enable_kvm:
        args.append("-enable-kvm")

    # Machine
    # -------

    args.append("-machine")
    args.append("q35")

    # UEFI firmware
    # -------------

    args.extend(alpy.qemu.get_uefi_firmware_args())

    # Memory
    # ------

    args.append("-m")
    args.append("512")

    # CPU
    # ---

    if enable_kvm:
        args.append("-cpu")
        args.append("host")

    # Graphics
    # --------

    args.append("-display")
    args.append("none")

    # QMP
    # ---

    args.extend(alpy.qemu.get_qmp_args())

    # Serial port
    # -----------

    args.extend(alpy.qemu.get_serial_port_args())

    # Storage
    # -------

    args.append("-blockdev")
    args.append(f"node-name=id_block_hdd,driver=file,filename={hdd_image_path}")

    args.append("-device")
    args.append("virtio-blk-pci,drive=id_block_hdd")

    # Boot
    # ----

    args.append("-boot")
    args.append("order=c")

    # Network
    # -------

    args.extend(alpy.qemu.get_network_interfaces_args(tap_interfaces))

    return args


@contextlib.contextmanager
def user_session(console, qmp):
    alpy.qemu.start_virtual_cpu(qmp)
    alpine.boot(console)
    alpy.qemu.read_events(qmp)
    alpine.login(console)
    try:
        yield
    finally:
        alpine.shutdown(console, PROMPT, qmp)


def configure(console, timeout):
    alpy.remote_shell.upload_and_execute_script(
        console, PROMPT, "configure-rabbit", timeout
    )


Config = collections.namedtuple(
    "Config",
    ["enable_kvm", "hdd_image_path", "iproute2_image", "link_count", "timeout"],
)


def collect_config(*, link_count):
    parser = argparse.ArgumentParser()
    add_common_arguments(parser)
    args = parser.parse_args()
    return Config(
        enable_kvm=not args.disable_kvm,
        hdd_image_path=args.hdd_image_path,
        iproute2_image=os.environ.get(
            "IMAGE_IPROUTE2",
            "registry.gitlab.com/abogdanenko/alpy/iproute2:latest",
        ),
        link_count=link_count,
        timeout=args.timeout,
    )


Resources = collections.namedtuple(
    "Resources", ["console", "docker_client", "qmp"]
)


@contextlib.contextmanager
def run(config):
    tap_interfaces = [f"carrot{i}" for i in range(config.link_count)]
    qemu_args = get_qemu_args(
        enable_kvm=config.enable_kvm,
        hdd_image_path=config.hdd_image_path,
        tap_interfaces=tap_interfaces,
    )
    docker_client = docker.from_env()
    try:
        skeleton = alpy.node.make_skeleton(
            busybox_image=IMAGE_BUSYBOX,
            docker_client=docker_client,
            iproute2_image=config.iproute2_image,
            tap_interfaces=tap_interfaces,
            timeout=config.timeout,
        )

        qemu_with_skeleton = alpy.run.qemu_with_skeleton(
            skeleton=skeleton, qemu_args=qemu_args, timeout=config.timeout
        )
        with alpy.utils.print_test_result():
            with alpy.qemu.temporary_copy_ovmf_vars():
                with qemu_with_skeleton as qmp:
                    with alpy.console.connect(
                        timeout=config.timeout
                    ) as console:
                        with alpy.utils.trace_test_environment():
                            yield Resources(console, docker_client, qmp)
    finally:
        docker_client.close()
