#!/usr/bin/env python3
# SPDX-License-Identifier: GPL-3.0-or-later

"""This program is a test for network virtual appliance "rabbit".

The test checks that rabbit can load-balance web traffic.

.. code:: text

   +----------------------------------------------------------+
   |                                                          |
   |                         rabbit                           |
   |                                                          |
   | 192.168.0.1/24      192.168.1.1/24        192.168.2.1/24 |
   |                                                          |
   +-------+--------------------+--------------------+--------+
           |                    |                    |
           |                    |                    |
           |                    |                    |
   +-------+--------+   +-------+--------+   +-------+--------+
   |                |   |                |   |                |
   | 192.168.0.2/24 |   | 192.168.1.2/24 |   | 192.168.2.2/24 |
   |                |   |                |   |                |
   | node0          |   | node1          |   | node2          |
   |                |   |                |   |                |
   +----------------+   +----------------+   +----------------+

Steps
-----

1. Configure network interfaces of all nodes.
2. Configure web server (httpd) on node 1 and node 2. Web server on node 1
   serves the following text: "Hello from node 1". Web server on node 2 serves
   the following text: "Hello from node 2".
3. Start web servers.
4. Configure rabbit network interfaces.
5. Configure rabbit to load-balance HTTP requests between node 1 and node 2.
6. Download (wget) web page http://192.168.0.1 from node 0 twice.
7. Check that the pages downloaded contain both responses: "Hello from node 1",
   "Hello from node 2".

Reference
---------

1. `Using nginx as HTTP load balancer
   <http://nginx.org/en/docs/http/load_balancing.html>`_

2. `httpd.c networking - busybox
   <https://git.busybox.net/busybox/tree/networking/httpd.c>`_

3. `Nginx - Alpine Linux <https://wiki.alpinelinux.org/wiki/Nginx>`_
"""

import contextlib
import logging

import alpy.container
import alpy.utils

import carrot


def main():
    config = carrot.collect_config(link_count=3)

    alpy.utils.configure_logging()
    logger = logging.getLogger(__name__)
    logger.info(
        "Test description: Check that rabbit load-balances web traffic."
    )

    with carrot.run(config) as resources:
        configure_nodes(resources.docker_client, config.timeout)
        with run_web_server(1, resources.docker_client, config.timeout):
            with run_web_server(2, resources.docker_client, config.timeout):
                with carrot.user_session(resources.console, resources.qmp):
                    carrot.configure(resources.console, config.timeout)
                    responses = [
                        run_web_client(resources.docker_client, config.timeout)
                        for node in [1, 2]
                    ]
        check_responses(responses)


def configure_nodes(docker_client, timeout):
    for node in [0, 1, 2]:
        alpy.container.add_ip_address(
            f"node{node}",
            f"192.168.{node}.2/24",
            docker_client=docker_client,
            image=carrot.IMAGE_BUSYBOX,
            timeout=timeout,
        )


def sample_response(node):
    return f"Hello from node {node}"


@contextlib.contextmanager
def run_web_server(node, docker_client, timeout):
    logger = logging.getLogger(__name__)
    context_logger = alpy.utils.make_context_logger(logger)

    shell_command = (
        f"echo -n '{sample_response(node)}' > index.html"
        " && exec httpd -f -vv"
    )

    container = None
    try:
        with context_logger(f"Start web server on node {node}"):
            container = docker_client.containers.create(
                carrot.IMAGE_BUSYBOX,
                ["sh", "-c", shell_command],
                network_mode=f"container:node{node}",
                init=True,
            )
            container.start()
        try:
            yield
        finally:
            with context_logger(f"Stop web server on node {node}"):
                alpy.container.stop(container, timeout)
    finally:
        if container:
            container.remove()


def run_web_client(docker_client, timeout):
    logger = logging.getLogger(__name__)
    context_logger = alpy.utils.make_context_logger(logger)
    url = "http://192.168.0.1"
    with context_logger("Go to " + url):
        container = docker_client.containers.create(
            carrot.IMAGE_BUSYBOX,
            ["wget", "-S", "-O", "-", url],
            network_mode="container:node0",
        )
        try:
            container.start()
            alpy.container.close(container, timeout)
            response = container.logs(stderr=False).decode()
            logger = logging.getLogger(__name__)
            logger.info("Response: " + response)
            return response
        finally:
            container.remove()


def check_responses(responses):
    logger = logging.getLogger(__name__)
    context_logger = alpy.utils.make_context_logger(logger)
    with context_logger("Check responses"):
        for node in [1, 2]:
            if sample_response(node) not in responses:
                raise RuntimeError(f"Missing response from node {node}")


if __name__ == "__main__":
    main()
