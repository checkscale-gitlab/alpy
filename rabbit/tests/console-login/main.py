#!/usr/bin/env python3
# SPDX-License-Identifier: GPL-3.0-or-later

"""This program is a test for network virtual appliance "rabbit".

The test checks that a user can log in to rabbit via console.
"""

import logging

import alpy.container
import alpy.utils

import carrot


def main():
    config = carrot.collect_config(link_count=0)

    alpy.utils.configure_logging()
    logger = logging.getLogger(__name__)
    logger.info("Test description: Login via console.")

    with carrot.run(config) as resources:
        with carrot.user_session(resources.console, resources.qmp):
            pass


if __name__ == "__main__":
    main()
