#!/usr/bin/env python3
# SPDX-License-Identifier: GPL-3.0-or-later

"""This program is a test for network virtual appliance "rabbit".

The test checks that when rabbit is turned on a bootloader starts.
"""

import logging

import alpy.utils

import carrot


def main():
    config = carrot.collect_config(link_count=0)

    alpy.utils.configure_logging()
    logger = logging.getLogger(__name__)
    logger.info("Test description: Start bootloader.")

    with carrot.run(config) as resources:
        alpy.qemu.start_virtual_cpu(resources.qmp)

        context_logger = alpy.utils.make_context_logger(logger)

        with context_logger("Wait for GRUB"):
            resources.console.expect_exact("Welcome to GRUB!")
            resources.console.expect_exact("\n")


if __name__ == "__main__":
    main()
