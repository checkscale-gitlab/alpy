# SPDX-License-Identifier: GPL-3.0-or-later

import logging

import alpy.console
import alpy.qemu
import alpy.utils


def boot(console):

    logger = logging.getLogger(__name__)
    context_logger = alpy.utils.make_context_logger(logger)

    with context_logger("Wait for the system to boot"):
        console.expect_exact("Welcome to GRUB!")
        console.expect_exact("OpenRC")
        while console.expect_exact(["Welcome to Alpine Linux", "ok"]):
            pass
        console.expect_exact("\n")


def login(console):

    logger = logging.getLogger(__name__)
    context_logger = alpy.utils.make_context_logger(logger)

    with context_logger("Login to the system"):
        console.expect_exact("login: ")
        console.sendline("root")
        console.expect_exact("Welcome to Alpine!")
        console.expect_exact("\n")


def shutdown(console, prompt, qmp):

    alpy.qemu.read_events(qmp)

    logger = logging.getLogger(__name__)
    context_logger = alpy.utils.make_context_logger(logger)

    with context_logger("Initiate system shutdown"):
        console.expect_exact(prompt)
        console.sendline("poweroff")
        console.expect_exact("The system is going down")
        console.expect_exact("\n")

    with alpy.console.read_in_background(console):
        alpy.qemu.wait_shutdown(qmp)

    alpy.console.flush_log(console)
