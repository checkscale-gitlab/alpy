#!/usr/bin/env python3
# SPDX-License-Identifier: GPL-3.0-or-later

import os
import subprocess
import sys

import docker

import utils


def main():

    utils.configure_logging()
    utils.install_alpy_wheel()

    with utils.run_docker_daemon():
        docker_client = docker.from_env()
        try:
            if "CI_REGISTRY" in os.environ:
                docker_client.login(
                    username=os.environ["CI_REGISTRY_USER"],
                    password=os.environ["CI_REGISTRY_PASSWORD"],
                    registry=os.environ["CI_REGISTRY"],
                )
            docker_client.images.pull("busybox:latest")
            docker_client.images.pull(os.environ["IMAGE_IPROUTE2"])
            subprocess.run(
                [
                    sys.executable,
                    "-m",
                    "coverage",
                    "run",
                    "--branch",
                    "--source",
                    "alpy",
                    "ping-linux-guest",
                    "--disable-kvm",
                    "--timeout",
                    "30",
                    "--iso-image-path",
                    "../../alpine.iso",
                ],
                check=True,
                cwd="tests/integration",
            )
            subprocess.run(
                [sys.executable, "-m", "pytest", "--verbose", "--color", "yes"],
                check=True,
                cwd="tests/integration",
            )
            subprocess.run(
                [sys.executable, "-m", "coverage", "report"],
                check=True,
                cwd="tests/integration",
            )
            subprocess.run(
                [sys.executable, "-m", "coverage", "html"],
                check=True,
                cwd="tests/integration",
            )
        finally:
            docker_client.close()


if __name__ == "__main__":
    main()
