# SPDX-License-Identifier: GPL-3.0-or-later

import contextlib
import logging
import subprocess
import sys
import time
from pathlib import Path

import docker


def wait_for_dockerd():

    timeout = 10
    logger = logging.getLogger(Path(__file__).name)
    time_start = time.time()
    logger.debug("Waiting for Docker daemon to start...")
    while True:
        try:
            docker_client = docker.from_env()
            break
        except docker.errors.DockerException:
            if time.time() > time_start + timeout:
                logger.error("Docker daemon is not responding")
                raise
            pause_duration = 0.5
            logger.debug(f"Sleeping for {pause_duration}s...")
            time.sleep(pause_duration)
    docker_client.close()
    logger.info("Docker daemon is running")


@contextlib.contextmanager
def run_docker_daemon():
    with open("dockerd.log", "wb") as log_file:
        process = subprocess.Popen(  # pylint: disable=consider-using-with
            ["dockerd", "--iptables=false"],
            stdout=log_file,
            stderr=subprocess.STDOUT,
        )
        wait_for_dockerd()
        try:
            yield
        finally:
            process.terminate()
            process.communicate()


def configure_logging():

    stream_handler = logging.StreamHandler()
    stream_formatter = logging.Formatter(
        fmt="{levelname:8} {name:22} {message}", style="{"
    )
    stream_handler.setLevel(logging.INFO)
    stream_handler.setFormatter(stream_formatter)

    file_handler = logging.FileHandler("script.log", mode="w")
    file_formatter = logging.Formatter(
        fmt="{relativeCreated:7.0f} {levelname:8} {name:22} {message}",
        style="{",
    )
    file_handler.setFormatter(file_formatter)
    root_logger = logging.getLogger()
    root_logger.setLevel(logging.DEBUG)
    root_logger.addHandler(stream_handler)
    root_logger.addHandler(file_handler)


def install_alpy_wheel():
    filename = next(Path("dist").glob("alpy-*.whl"))
    subprocess.run(
        [sys.executable, "-m", "pip", "install", filename], check=True
    )
