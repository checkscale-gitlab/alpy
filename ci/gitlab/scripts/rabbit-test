#!/usr/bin/env python3
# SPDX-License-Identifier: GPL-3.0-or-later

import contextlib
import os
import subprocess
import sys
from pathlib import Path

import docker

import utils


def pull_test_requirements(docker_client, test_path):
    if "CI_REGISTRY" in os.environ:
        docker_client.login(
            username=os.environ["CI_REGISTRY_USER"],
            password=os.environ["CI_REGISTRY_PASSWORD"],
            registry=os.environ["CI_REGISTRY"],
        )

    # Start with engine requirements
    images = {"busybox:latest", os.environ["IMAGE_IPROUTE2"]}

    requirements_path = Path(test_path) / "docker-requirements"
    if requirements_path.exists():
        with requirements_path.open() as f:
            for line in f:
                images.add(line[:-1])

    for image in images:
        docker_client.images.pull(image)


def append_pythonpath_env(env, path):
    key = "PYTHONPATH"
    env[key] = env[key] + ":" + path if key in env else path


def run_test(test_name):
    env = os.environ.copy()
    append_pythonpath_env(env, "../..")
    subprocess.run(
        [
            sys.executable,
            "main.py",
            "--disable-kvm",
            "--timeout",
            "30",
            "--hdd-image-path",
            "../../rabbit.img",
        ],
        check=True,
        cwd="rabbit/tests/" + test_name,
        env=env,
    )


@contextlib.contextmanager
def setup_test_environment(test_name):
    with utils.run_docker_daemon():
        docker_client = docker.from_env()
        try:
            pull_test_requirements(docker_client, "rabbit/tests/" + test_name)
            yield
        finally:
            docker_client.close()


def main():

    utils.configure_logging()
    utils.install_alpy_wheel()
    test_name = os.environ["CI_JOB_NAME"]

    with setup_test_environment(test_name):
        run_test(test_name)


if __name__ == "__main__":
    main()
