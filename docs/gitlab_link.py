# SPDX-License-Identifier: GPL-3.0-or-later

import importlib
import inspect
import logging
import os


def make_gitlab_link(*, project_url, commit_sha, filename, line_number):
    return f"{project_url}/blob/{commit_sha}/{filename}#L{line_number}"


def linkcode_resolve(domain, info):
    logger = logging.getLogger(__name__)
    if domain != "py":
        return None
    if not info["module"]:
        return None
    module = info["module"]
    fullname = info["fullname"]
    logger.debug(f'Looking for object "{fullname}" in module {module}')
    module_object = importlib.import_module(module)
    obj = module_object
    for name in fullname.split("."):
        obj = obj.__dict__[name]
    logger.debug(f"Found {obj}")
    if not (inspect.isfunction(obj) or inspect.isclass(obj)):
        logger.debug(f"Skip {obj}. It is not a function or a class.")
        return None
    line_number = inspect.getsourcelines(obj)[1]
    filename = module.replace(".", "/") + ".py"
    return make_gitlab_link(
        project_url=os.environ["CI_PROJECT_URL"],
        commit_sha=os.environ["CI_COMMIT_SHA"],
        filename=filename,
        line_number=line_number,
    )


def setup(app):
    app.config.linkcode_resolve = linkcode_resolve
    return {"parallel_read_safe": True, "parallel_write_safe": True}
