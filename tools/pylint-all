#!/usr/bin/env python3
# SPDX-License-Identifier: GPL-3.0-or-later

"""Run Pylint on all Python files.

usage: tools/pylint-all [EXTRA_PYLINT_ARGUMENT...]
"""

import concurrent.futures
import subprocess
import sys
from pathlib import Path

import utils


def main():
    executor = (  # pylint: disable=consider-using-with
        concurrent.futures.ProcessPoolExecutor()
    )
    try:
        futures = []

        for path in utils.iterate_over_files_recursively(Path(".")):
            if utils.is_python_file(path):
                args = (
                    [sys.executable, "tools/pylint-one"]
                    + sys.argv[1:]
                    + [str(path)]
                )
                futures.append(
                    executor.submit(
                        subprocess.run,
                        args,
                        check=False,
                        stderr=subprocess.STDOUT,
                        stdout=subprocess.PIPE,
                    )
                )

        first_nonzero_exit_code = 0

        for future in futures:
            completed_process = future.result()
            print(*completed_process.args, flush=True)
            if first_nonzero_exit_code == 0:
                first_nonzero_exit_code = completed_process.returncode
            sys.stdout.buffer.write(completed_process.stdout)
    finally:
        if sys.version_info >= (3, 9):
            # pylint: disable=unexpected-keyword-arg
            executor.shutdown(cancel_futures=True)
        else:
            executor.shutdown()

    sys.exit(first_nonzero_exit_code)


if __name__ == "__main__":
    main()
