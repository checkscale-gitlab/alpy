#!/usr/bin/env python3
# SPDX-License-Identifier: GPL-3.0-or-later

import argparse
import subprocess
import sys


def parse_args():

    parser = argparse.ArgumentParser(
        description="Sort imports using isort utility",
        epilog=(
            "Return 0 if nothing is changed, otherwise return non-zero code."
        ),
    )

    parser.add_argument(
        "--diff",
        action="store_true",
        help=(
            "Don't write the files back, just output a diff for each file on "
            "stdout."
        ),
    )

    return parser.parse_args()


def main():
    args = parse_args()
    isort_args = [
        sys.executable,
        "-m",
        "isort",
        "--force-single-line-imports",
    ]

    if args.diff:
        isort_args += ["--check-only", "--color", "--diff"]

    completed_process1 = subprocess.run(
        isort_args
        + [
            "--extend-skip",
            "rabbit",
            "--project",
            "common",
            "--project",
            "utils",
            ".",
        ],
        check=False,
    )

    completed_process2 = subprocess.run(
        isort_args
        + [
            "--thirdparty",
            "alpy",
            "--project",
            "alpine",
            "--project",
            "carrot",
            "rabbit",
        ],
        check=False,
    )

    if completed_process1.returncode or completed_process2.returncode:
        sys.exit(1)


if __name__ == "__main__":
    main()
