#!/usr/bin/env python3
# SPDX-License-Identifier: GPL-3.0-or-later

import logging
import sys
import urllib.request
from pathlib import Path

import yaml


# Download the latest Alpine ISO image into the working directory.
def main():

    logging.basicConfig(
        level=logging.DEBUG, style="{", format="{levelname:8} {message}"
    )

    logger_name = Path(sys.argv[0]).name
    logger = logging.getLogger(logger_name)

    releases_url_prefix = (
        "http://dl-cdn.alpinelinux.org/alpine/latest-stable/releases/x86_64/"
    )

    try:
        releases_url = releases_url_prefix + "latest-releases.yaml"
        logger.debug("URL: " + releases_url)
        logger.debug("Downloading release descriptions...")
        releases_filename, _ = urllib.request.urlretrieve(releases_url)

        logger.debug("Parsing the descriptions to find ISO image filename...")
        with open(releases_filename, encoding="ascii") as releases_file:
            releases_list = yaml.safe_load(releases_file)

        for release in releases_list:
            if release["flavor"] == "alpine-virt":
                iso_filename = release["iso"]

        iso_url = releases_url_prefix + iso_filename
        logger.info("URL: " + iso_url)
        logger.info("Downloading the image into the working directory...")
        local_filename = "alpine.iso"
        urllib.request.urlretrieve(iso_url, filename=local_filename)

    except:
        logger.error("Failed to download the image")
        raise
    finally:
        urllib.request.urlcleanup()
    logger.info(f"ISO image saved as {local_filename!r}")


if __name__ == "__main__":
    main()
